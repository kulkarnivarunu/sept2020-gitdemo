// Factorial of numbers
#include <stdio.h>

int factorial(int n)
{
    //return n*n;
    int i,r=1;
    for(i=1;i<=n;i++)
     r = r * i ;
    return r;
}

int main()
{
    // Declaration 
    int num,factorial(int);

    // Input Section
    printf("Enter a NUmber : ");
    scanf("%d",&num);

    // Output Section
    printf("Factorial of %d is %d.\n",num,factorial(num));

    return 0;
}