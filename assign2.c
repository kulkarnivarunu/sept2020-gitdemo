// Subtraction of two Numbers
#include <stdio.h>

int main()
{
    // Declaration 
    int a,b;

    // Input Section
    printf("Enter Two Numbers : ");
    scanf("%d%d",&a,&b);

    // Output Section 
    printf("Addition of %d and %d is %d.\n",a,b,a-b);

    return 0;
}